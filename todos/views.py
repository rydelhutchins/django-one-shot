from django.shortcuts import render
from todos.models import TodoList, TodoItem

# Create your views here.
def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_lists": todo_lists,
    }
    return render(request, "todos/lists.html", context)

def todo_list_detail(request, id):
    todo_list = TodoList.objects.get(id=id)
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/detail.html", context)